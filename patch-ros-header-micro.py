import os

libdeps_dir = "./.piolibdeps"

target_lines = [
    "namespace ros",
    "{",
    "#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega168__)",
    "/* downsize our buffers */",
    "typedef NodeHandle_<ArduinoHardware, 6, 6, 150, 150> NodeHandle;"
]

for directory in os.listdir(libdeps_dir):
    ros_header_path = os.path.join(libdeps_dir, directory, "ros.h")
    if directory.startswith("rosserial_arduino") and os.path.isfile(ros_header_path):
        content_lines = []
        with open(ros_header_path, "r") as f:
            content_lines = f.readlines()

        target_line_found_pos = -1
        for i, line in enumerate(content_lines):
            if line.strip() == target_lines[0]:
                all_lines_found = True
                for j, target_line in enumerate(target_lines):
                    if content_lines[i + j].strip() != target_line:
                        all_lines_found = False
                        break
                    if all_lines_found:
                        target_line_found_pos = i + j
                        break

        if target_line_found_pos != -1:
            patched_line = content_lines[target_line_found_pos + 2].rstrip() + " || defined(__AVR_ATmega32U4__)\n"
            print(patched_line)
            content_lines[target_line_found_pos + 2] = patched_line

            with open(ros_header_path, "w") as f:
                f.writelines(content_lines)
            
            print("patched {}".format(ros_header_path))
            break


