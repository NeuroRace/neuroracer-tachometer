#define USE_USBCON

#include <ros.h>
#include <geometry_msgs/TwistStamped.h>
#include "utils/RevolutionCounter.hpp"

static const uint16_t WRITE_DELAY_MS = 1000;
static const uint16_t USB_SERIAL_BAUD_RATE = 57600;
static const float METERS_PER_INTERRUPT = 0.112;

static const uint8_t HALL_SENSOR_PIN = 2;

static RevolutionCounter counter;

static const char* SPEED_OUTPUT_TOPIC = "/nr/tachometer/output/speed";
//static const uint32_t QUEUE_SIZE = 1;

ros::NodeHandle nh;

geometry_msgs::TwistStamped speed_msg;
ros::Publisher speed_publisher(SPEED_OUTPUT_TOPIC, &speed_msg);

int interrupt_counter = 0;

void handle_hall_interrupt() {
    counter.update();
    interrupt_counter++;
}

void setup() {
    pinMode(HALL_SENSOR_PIN, INPUT);
    Serial.begin(USB_SERIAL_BAUD_RATE);

    nh.initNode();
    nh.advertise(speed_publisher);

    attachInterrupt(static_cast<uint8_t>(digitalPinToInterrupt(HALL_SENSOR_PIN)), handle_hall_interrupt, FALLING);
}

void loop() {
    float rps = counter.get_current_revolutions();
    float speed = rps * METERS_PER_INTERRUPT;

    geometry_msgs::TwistStamped speed_msg;
    speed_msg.twist.linear.x = speed;

    speed_publisher.publish(&speed_msg);

    nh.spinOnce();
    delay(WRITE_DELAY_MS);
}
