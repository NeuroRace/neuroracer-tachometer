#include "RevolutionCounter.hpp"

RevolutionCounter::RevolutionCounter() : last_update_time(0) {};

unsigned long RevolutionCounter::calc_time_difference(uint32_t update_time) {
    uint32_t time_difference = update_time - last_update_time;

    if (last_update_time > update_time) {
        time_difference = U_LONG_MAX - last_update_time + update_time;
    }

    return time_difference;
}

void RevolutionCounter::update() {
    uint32_t current_time = micros();
    history.put(calc_time_difference(current_time));
    last_update_time = current_time;

    //interrupt_counter = 10.f;
}

float RevolutionCounter::get_current_revolutions() {
    return get_current_revolutions(true);
}

float RevolutionCounter::get_current_revolutions(bool disable_interrupts_during_copy) {
    uint32_t buffer[HISTORY_SIZE];

    if (disable_interrupts_during_copy) {
        noInterrupts();
    }

    size_t written_values = history.previous(buffer, HISTORY_SIZE);
    uint32_t current_to_last = calc_time_difference(micros());

    if (disable_interrupts_during_copy) {
        interrupts();
    }

    uint32_t time_diff_sum = 0;
    int used_interrupt_spans = 0;
    for (int i = 0; i < written_values; i++) {
        uint32_t current_backtrace_span = current_to_last + time_diff_sum + buffer[i];
        if (i != 1 && current_backtrace_span > MAX_HISTORY_BACKTRACE_TIME) {
            break;
        }
        time_diff_sum += buffer[i];
        used_interrupt_spans = i + 1;
    }

    if (used_interrupt_spans == 0) {
        return 0.f;
    }

    return used_interrupt_spans / ((double) time_diff_sum / (double) ONE_SECOND_IN_MICROS);
}