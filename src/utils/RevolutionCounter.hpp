#include "stddef.h"
#include "Arduino.h"
#include "RingBuffer.hpp"

static const uint32_t ONE_SECOND_IN_MICROS = 1000000L;
static const uint32_t U_LONG_MAX = 4294967295L;
static const size_t HISTORY_SIZE = 10;
static const uint32_t MAX_HISTORY_BACKTRACE_TIME = 1000000L;

class RevolutionCounter {
    RingBuffer<uint32_t, HISTORY_SIZE> history;

    uint32_t last_update_time;

    uint32_t calc_time_difference(uint32_t update_time);

public :
    RevolutionCounter();
    void update();
    float get_current_revolutions();
    float get_current_revolutions(bool disable_interrupts_during_copy);
};
