#include "stddef.h"
#include "Arduino.h"

template<typename T, size_t N>
class RingBuffer {
public:
    RingBuffer();

    T operator[](size_t index) const;

    T previous(size_t index) const;

    size_t previous(T *buffer, size_t buffer_length) const;

    bool try_previous(size_t index, T *t) const;

    void put(const T &t);

    bool is_full() const;

    void operator<<(const T &t);

    size_t size() const;

private:
    size_t position;
    T buffer[N];
    bool full;
};

template<typename T, size_t N>
RingBuffer<T, N>::RingBuffer()
        : position(0), full(false) {}

template<typename T, size_t N>
T RingBuffer<T, N>::operator[](size_t index) const {
    return buffer[index];
}

template<typename T, size_t N>
T RingBuffer<T, N>::previous(size_t index) const {
    size_t access_index = position - index;

    if (index > position) {
        access_index = N - index;
    }

    return buffer[access_index];
}

template<typename T, size_t N>
size_t RingBuffer<T, N>::previous(T *buffer, size_t buffer_length) const {
    size_t max_length = full ? buffer_length : min(buffer_length, position);

    for (int i = 0; i < max_length; i++) {
        buffer[i] = previous(i);
    }

    return max_length;
}

template<typename T, size_t N>
bool RingBuffer<T, N>::try_previous(size_t index, T *t) const {
    if (!full && position - index <= 0) {
        return false;
    }

    *t = buffer[(position - index) % N];
    return true;
}

template<typename T, size_t N>
void RingBuffer<T, N>::put(const T &t) {
    if (!full && position + 1 >= N) {
        full = true;
    }

    position = (position + 1) % N;
    buffer[position] = t;

}

template<typename T, size_t N>
bool RingBuffer<T, N>::is_full() const {
    return full;
}
